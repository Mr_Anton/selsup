package com.anton.afnasiev.selsup;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import com.google.gson.Gson;

public class CrptApi {

    private final Semaphore requestSemaphore;
    private final HttpClient httpClient;
    private final Gson gson;
    private final ScheduledExecutorService scheduler;

    public CrptApi(TimeUnit timeUnit, int requestLimit) {
        this.requestSemaphore = new Semaphore(requestLimit);
        this.httpClient = HttpClient.newHttpClient();
        this.gson = new Gson();
        this.scheduler = Executors.newScheduledThreadPool(1);
        scheduleSemaphoreRelease(timeUnit, requestLimit);
    }

    private void scheduleSemaphoreRelease(TimeUnit timeUnit, int requestLimit) {
        long periodInMillis = timeUnit.toMillis(1);
        scheduler.scheduleAtFixedRate(() -> requestSemaphore.release(requestLimit - requestSemaphore.availablePermits()), periodInMillis, periodInMillis, TimeUnit.MILLISECONDS);
    }

    public void createDocument(Object document, String signature) throws InterruptedException, IOException {
        String requestBody = gson.toJson(document);
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://ismp.crpt.ru/api/v3/lk/documents/create"))
                .header("Content-Type", "application/json")
                .header("Signature", signature)
                .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                .build();

        requestSemaphore.acquire();

        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } finally {
            requestSemaphore.release();
        }
    }

    // Метод закрытия для остановки scheduler и освобождения ресурсов
    public void close() {
        scheduler.shutdown();
        try {
            if (!scheduler.awaitTermination(60, TimeUnit.SECONDS)) {
                scheduler.shutdownNow();
            }
        } catch (InterruptedException e) {
            scheduler.shutdownNow();
        }
    }
}
